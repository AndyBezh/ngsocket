# ngSocket - simple Socket.io with AngularJS test app

**Learn:**

 - Socket.io basics
 - Clients tracking on server
 - Rooms 
 - Namespaces


## How to run

```bash
cd ngsocket
python -m SimpleHTTPServer
DEBUG=* node server.js
```
