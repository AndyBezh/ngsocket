var io = require('socket.io')();


io.listen(3000);

var sock_ids = [];
var sock_map = {};

io.on('connection', function(socket){
	console.log('connected');

	sock_ids.push(socket.id);
	console.log(sock_ids);

	socket.on('disconnect', function(){
		console.log('disconnect');

		indx = sock_ids.indexOf(socket.id)
		sock_ids.splice(indx, 1)
		console.log(sock_ids);

		if (sock_map.hasOwnProperty(socket.id)) {
			delete sock_map[socket.id];
		}
		console.log(sock_map);
	});

	socket.on('message', function(data){
		console.log(data);
	});

	socket.on('user_connect', function(data){		
		console.log(data);
		
		sock_map[socket.id] = data.user_id;
		console.log(sock_map);
			
		io.to(socket.id).emit('server-room-emit', {data: 'Welcome ' + data.user_id});
	});

	socket.emit('server-emit', {data: 'server data'});
	




});

