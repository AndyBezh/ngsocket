/**
* ngSocket Module
*
* Description
*/
var ngSocket = angular.module('ngSocket', []);

ngSocket.controller('ngSocketController', function($scope){
	$scope.name = 'Andrew';	

	var socket = io.connect("http://localhost:3000");
	console.log(socket);

	socket.emit('message', {data: 42});

	socket.emit('user_connect', {user_id: Math.floor((Math.random() * 100) + 1)});

})